import { Column, Entity, JoinColumn, Long, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Audit } from '../../commons/entity/audit.entity';
import { PaymentDetails } from './payment-details.entity';

@Entity()
export class Payment extends Audit {

  @PrimaryGeneratedColumn()
  id: Long;

  @ApiProperty()
  @Column()
  userId: number;

  @ApiProperty()
  @Column({ default: null })
  paymentEmail: string;

  @ApiProperty()
  @Column({ default: 0, type: 'float' })
  grossAmount: number;

  @ApiProperty()
  @Column({ default: 0, type: 'float' })
  joinARoomFee: number;

  @ApiProperty()
  @Column({ default: 0, type: 'float' })
  payPalFee: number;

  @ApiProperty()
  @Column({ default: 0, type: 'float' })
  netAmount: number;

  @ApiProperty()
  @Column({ default: 0, type: 'float' })
  totalPaid: number;

  @ApiProperty()
  @Column({ default: 0, type: 'float' })
  balance: number;

  @ApiProperty()
  @Column({ default: 'C' })
  creditOrDebit: string;

  @ApiProperty()
  @Column({ default: 'paypal' })
  preferredProvider: string;

  @OneToOne(type => PaymentDetails, paymentDetail => paymentDetail.payment)
  @JoinColumn()
  paymentDetails: PaymentDetails;

}
