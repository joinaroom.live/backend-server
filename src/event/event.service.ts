import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Event } from 'src/event/entity/events.entity';
import { UserService } from 'src/user/user.service';
import * as currentWeekNumber from 'current-week-number';
import { User } from '../user/entity/user.entity';
import * as twilio from 'twilio';
import { ConfigService } from '@nestjs/config';

class WeeklyEvents {
  week: any;
  events: Event[];
}

@Injectable()
export class EventService {
  constructor(@InjectRepository(Event) private eventRepository: Repository<Event>,
              private userService: UserService) {
  }

  async findAllEvents(): Promise<any> {
    return this.sortEventsPerWeek(await this.eventRepository.find());
  }

  async findPaymentEvents(userId: number): Promise<any> {
    const user = new User();
    user.id = userId;
    const events = await this.eventRepository.find({ user: user });
    const weeks = new Set();
    const weeklyEvents: WeeklyEvents[] = [];
    events.map(it => {
      weeks.add(currentWeekNumber(it.eventDate));
    });

    const wks = Array.from(weeks).sort();
    for (const week of wks) {
      const we = new WeeklyEvents();
      const e = events.filter(event => currentWeekNumber(event.eventDate) == week);
      we.week = week;
      we.events = e;
      weeklyEvents.push(we);
    }
    console.log(weeklyEvents);
    return weeklyEvents;
  }

  async findMyEvents(userId: number): Promise<any> {
    const user = new User();
    user.id = userId;
    const myEvents = await this.findEventsByUser(user);
    const subscribedEvents: Event[] = [];

    const subscribed = await this.eventRepository.find();
    subscribed.forEach(event => {
      if (event.subscribers) {
        event.subscribers.forEach(subscriber => {
          if (subscriber.userId == userId) {
            subscribedEvents.push(event);
          }
        });
      }
    });

    return this.sortEventsPerWeek(myEvents.concat(subscribedEvents));
  }

  async findById(id: number): Promise<Event> {
    return await this.eventRepository.findOne(id);
  }

  // TODO Sam is this good?
  async updateEvent(event: Event): Promise<Event> {
    return await this.eventRepository.save(event);
  }

  async deleteEventById(eventid: number): Promise<any> {
    const eventToRemove = await this.eventRepository.findOne(eventid);
    return await this.eventRepository.remove(eventToRemove);
  }

  async findEventsByUser(user: User) {
    return await this.eventRepository.find({ where: { user: user } });
  }

  async createEvent(event: Event, userId: number): Promise<Event> {
    event.user = await this.userService.findUserById(userId);
    /*
    const client = twilio(process.env.TWILIO_SID, process.env.TWILIO_SECRET);
    event = await client.video.rooms.create({
      uniqueName: event.eventName + '_' + new Date().getTime(),
      videoCodecs: ['H264'],
      type: 'group',
      recordParticipantsOnConnect: false,
      maxParticipants: 50,
    }).then(room => {
      event.uniqueRoomName = room.uniqueName;
      event.recordingUrl = room.links['recordings'];
      event.participantsUrl = room.links['participants'];
      return event;
    }).catch(error => {
      throw new HttpException({
        status: HttpStatus.BAD_REQUEST,
        error: 'Room already exists',
        timestamp: new Date().toISOString(),
        path: '/events',
      }, HttpStatus.BAD_REQUEST);
    });
    */
    return await this.eventRepository.save(event);
  }

  sortEventsPerWeek(events: Event[]) {
    const weeks = new Set();
    // eslint-disable-next-line prefer-const
    let weeklyEvents: WeeklyEvents[] = [];

    events.forEach(event => {
      if (currentWeekNumber(event.eventDate) >= currentWeekNumber()) {
        weeks.add(currentWeekNumber(event.eventDate));
      }
    });

    const wks = Array.from(weeks).sort();
    for (const week of wks) {
      const we = new WeeklyEvents();
      const e = events.filter(event => parseInt(event.endTime) > (new Date().getTime()))
        .filter(event => currentWeekNumber(event.eventDate) == week);
      we.week = week;
      we.events = e;
      weeklyEvents.push(we);
    }

    console.log(weeklyEvents);

    if (weeklyEvents.length == 1 && weeklyEvents[0].events.length == 0) {
      return [];
    }

    return weeklyEvents;
  }

}
