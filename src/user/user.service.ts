import { Injectable } from '@nestjs/common';
import { User } from './entity/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as CryptoJS from 'crypto-js';
import { NotificationsService } from 'src/notifications/notifications.service';


@Injectable()
export class UserService {

  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    private notificationsService: NotificationsService,
  ) {
  }

  async findOne(email: string): Promise<User> {
    return await this.userRepository.findOne({
      where: { email },
      select: ['id', 'email', 'username', 'password', 'role'],
    });
  }

  async userExists(username: string): Promise<boolean> {
    const user = await this.userRepository.findOne({ where: { username: username } });
    if (user) {
      return true;
    } else {
      return false;
    }
  }

  async isUserVerified(token: string): Promise<any> {
    const buffer = new Buffer(token, 'base64');
    const decodedToken = buffer.toString();
    const encryptedEmail = CryptoJS.AES.decrypt(decodedToken, process.env.CRYPTO_KEY);
    const userEmail = encryptedEmail.toString(CryptoJS.enc.Utf8);

    if (userEmail) {
      const user = await this.userRepository.findOne({ where: { email: userEmail } });
      if (user) {
        return JSON.stringify({ status: user.accountStatus });
      } else {
        return JSON.stringify({ message: 'User not exist' });
      }
    } else {
      return JSON.stringify({ message: 'incorrect-token' });
    }
  }

  async isUserVerifiedByEmail(userEmail: string): Promise<any> {
    if (userEmail) {
      const user = await this.userRepository.findOne({ where: { email: userEmail } });
      if (user) {
        return JSON.stringify({ status: user.accountStatus });
      } else {
        return JSON.stringify({ message: 'User not exist' });
      }
    }
  }

  async verifyUserEmail(token: string): Promise<User> {
    const buffer = new Buffer(token, 'base64');
    const decodedToken = buffer.toString();
    const encryptedEmail = CryptoJS.AES.decrypt(decodedToken, process.env.CRYPTO_KEY);
    const userEmail = encryptedEmail.toString(CryptoJS.enc.Utf8);
    const user = await this.userRepository.findOne({ where: { email: userEmail } });
    const entity = Object.assign(new User(), user);
    entity.accountStatus = 'active';
    await this.notificationsService.sendSucessEmailVerification(entity);
    return this.userRepository.save(entity);
  }

  async saveUser(user: User): Promise<User | undefined> {
    console.log(user);
    const entity = Object.assign(new User(), user);
    if (entity.email == 'developer.njane@gmail.com' || entity.email == 'tanimhbk@gmail.com' || entity.email == 'test@test.com') {
      entity.role = 'ADMIN';
    }
    return this.userRepository.save(entity);
  }

  async findAllUsers(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async findUserById(id: number): Promise<User | undefined> {
    return await this.userRepository.findOne(id);
  }
}
