import { Module, HttpModule } from '@nestjs/common';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entity/user.entity';
import { UserController } from './user.controller';
import { PassportModule } from '@nestjs/passport';
import { NotificationsModule } from 'src/notifications/notifications.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), HttpModule, PassportModule, NotificationsModule],
  providers: [UserService],
  controllers: [UserController],
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  exports: [UserService, UserModule]
})
export class UserModule {}
