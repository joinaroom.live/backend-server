import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Audit } from 'src/commons/entity/audit.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Event } from 'src/event/entity/events.entity';
import { PaymentStatus } from '../../enums/payment-status';

@Entity()
export class Billing extends Audit {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @ApiProperty()
  eventId: number;

  @Column({ default: 0, type: 'float' })
  @ApiProperty()
  eventPrice: number;

  @Column({ default: 0, type: 'float' })
  withdrwalRequest: number;

  @Column({ default: 0, type: 'float' })
  previousWithdrwalRequest: number;

  @Column({ default: 0, type: 'float' })
  @ApiProperty()
  withdrawnAmount: number;

  @Column({ default: 0, type: 'float' })
  @ApiProperty()
  balance: number;

  @Column({ default: PaymentStatus.PENDING })
  @ApiProperty({ enum: PaymentStatus, enumName: 'PaymentStatus' })
  status: PaymentStatus;

  @OneToOne(type => Event, event => event.billing)
  @JoinColumn({ name: 'eventId' })
  event: Event;

}
