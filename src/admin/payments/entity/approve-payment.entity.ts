import { BeforeInsert, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Event } from '../../../event/entity/events.entity';
import { PaymentStatus } from '../../../enums/payment-status';
import { ApiProperty } from '@nestjs/swagger';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class ApprovePayment {

  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ enum: PaymentStatus, enumName: 'PaymentStatus' })
  @Column({ default: PaymentStatus.PENDING })
  status: PaymentStatus;

  @ApiProperty()
  @Column()
  payoutBatchId: string;

  @ApiProperty()
  @Column({ default: 0, type: 'float' })
  amount: number;

  @ApiProperty()
  @Column({ nullable: true })
  remark: string;

  @ApiProperty()
  @Column({ nullable: false })
  requestId: string;

  @ManyToOne(type => Event, event => event.approvePayment, { eager: true })
  event: Event;

  @BeforeInsert()
  async generateUniqueId() {
    this.requestId = uuidv4().toString();
  }
}
