import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventModule } from './event/event.module';
import { BillingModule } from './billing/billing.module';
import { LoggerMiddleware } from './middleware/logger.middleware';
import { PaymentModule } from './payment/payment.module';
import { VideoModule } from './video/video.module';
import { SubscriberModule } from './subscriber/subscriber.module';
import { OrderModule } from './order/order.module';
import { PaymentsModule } from './admin/payments/payments.module';
import { NotificationsModule } from './notifications/notifications.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    AuthModule,
    UserModule,
    EventModule,
    BillingModule,
    PaymentModule,
    VideoModule,
    SubscriberModule,
    OrderModule,
    PaymentsModule,
    NotificationsModule,
    ConfigModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes('*');
  }
}
