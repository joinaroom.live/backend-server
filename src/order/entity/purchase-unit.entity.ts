import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Audit } from '../../commons/entity/audit.entity';
import { Order } from './order.entity';

@Entity()
export class PurchaseUnits extends Audit {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  addressLine1: string;

  @Column({ nullable: true })
  addressLine2: string;

  @Column({ nullable: true })
  adminArea1: string;

  @Column({ nullable: true })
  adminArea2: string;

  @Column({ nullable: true })
  postalCode: string;

  @Column({ nullable: true })
  countryCode: string;

  @Column({ nullable: false })
  captureId: string;

  @Column({ nullable: true })
  status: string;

  @Column({ default: 0, type: 'float' })
  amount: number;

  @Column({ default: 0, type: 'float' })
  grossAmount: number;

  @Column({ default: 0, type: 'float' })
  payPalFee: number;

  @Column({ default: 0, type: 'float' })
  netAmount: number;

  @Column({ nullable: true })
  currencyCode: string;

  @ManyToOne(type => Order, order => order.purchaseUnits)
  order: Order;

}
