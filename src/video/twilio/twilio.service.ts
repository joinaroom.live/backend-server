import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as twilio from 'twilio';
import { TwilioVideo } from './entity/twilio-video.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserService } from 'src/user/user.service';
import { EventService } from 'src/event/event.service';

const AccessToken = twilio.jwt.AccessToken;
const VideoGrant = AccessToken.VideoGrant;

@Injectable()
export class TwilioService {

  constructor(
    @InjectRepository(TwilioVideo) videoRepository: Repository<TwilioVideo>,
    private userService: UserService,
    private eventService: EventService,
  ) {
  }

  async generateAccessToken(twilioVideo: TwilioVideo): Promise<any> {

    const user = await this.userService.findUserById(twilioVideo.userId);
    const event = await this.eventService.findById(twilioVideo.eventId);

    // if room is not already created, create the room here
    if (!event?.uniqueRoomName?.length) {

      const client = twilio(process.env.TWILIO_SID, process.env.TWILIO_SECRET);

      const updatedEvent = await client.video.rooms.create({
        uniqueName: event.eventName + '_' + new Date().getTime(),
        videoCodecs: ['H264'],
        type: 'group',
        recordParticipantsOnConnect: false,
        maxParticipants: 50,
      }).then(room => {
        event.uniqueRoomName = room.uniqueName;
        event.recordingUrl = room.links['recordings'];
        event.participantsUrl = room.links['participants'];

        return this.eventService.updateEvent(event);

      }).catch(error => {
        throw new HttpException({
          status: HttpStatus.BAD_REQUEST,
          error: 'Room already exists',
          timestamp: new Date().toISOString(),
          path: '/events',
        }, HttpStatus.BAD_REQUEST);
      });
    }
    // end of room creation

    if (user && event && user.username !== null && event.uniqueRoomName !== null) {
      const accessToken = new AccessToken(
        process.env.TWILIO_SID,
        process.env.TWILIO_API_KEY_SID,
        process.env.TWILIO_API_KEY_SECRET,
        { identity: user.username },
      );

      const grant = new VideoGrant({ room: event.uniqueRoomName });
      accessToken.addGrant(grant);
      const twilioAccessToken = accessToken.toJwt();

      return { twilioToken: twilioAccessToken };
    }

    return null;
  }
}
