/* eslint-disable @typescript-eslint/camelcase */
import { HttpException, HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { Email, Personalization, SendGridTemplate, To } from './dto/SendGridTemplate';
import { User } from '../user/entity/user.entity';
import { Event } from '../event/entity/events.entity';
import * as CryptoJS from 'crypto-js';
import { ConfigService } from '@nestjs/config';


const sgUrl = 'https://api.sendgrid.com/v3/mail/send';
const supportEmail = 'joinaroom2020@gmail.com';

@Injectable()
export class NotificationsService {
  constructor(private httpService: HttpService) {
  }

  async sendPasswordResetEmail(user: User) {
    const resetPasswordDto = new SendGridTemplate();
    resetPasswordDto.personalizations = [];
    const personalization = new Personalization();
    personalization.to = [];
    const to = new To(user.email);

    resetPasswordDto.from = new Email(supportEmail);
    personalization.to.push(to);
    personalization.dynamic_template_data = {
      'email': user.email,
      'name': user.username,
      'resetPasswordUrl': process.env.HOST + '/password-reset?email=' + user.email,
    };
    resetPasswordDto.personalizations.push(personalization);
    resetPasswordDto.template_id = 'd-9676e9da1e6f4858a86214bc41c59516';


    return this.httpService.post(sgUrl, resetPasswordDto, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + process.env.SG_API_KEY,
      },
    }).toPromise()
      .then(response => {
        return true;
      }).catch(error => {
        throw new HttpException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.toString(),
          timestamp: new Date().toISOString(),
          path: '/auth/forgot/password',
        }, HttpStatus.INTERNAL_SERVER_ERROR);
      });
  }

  async sendPaymentConfirmationEmail(event: Event, user: User) {
    const resetPasswordDto = new SendGridTemplate();
    resetPasswordDto.personalizations = [];
    const personalization = new Personalization();
    personalization.to = [];
    const to = new To(user.email);

    resetPasswordDto.from = new Email(supportEmail);
    personalization.to.push(to);
    personalization.dynamic_template_data = {
      'name': user.username,
      'date': event.eventDate,
      'roomLink': process.env.HOST + '/rooms/' + event.id,
      'myRoomLink': process.env.HOST + '/my-rooms',
    };
    resetPasswordDto.personalizations.push(personalization);
    resetPasswordDto.template_id = 'd-bf25dbac38994ef597c2d01e0d7a308e';


    return this.httpService.post(sgUrl, resetPasswordDto, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + process.env.SG_API_KEY,
      },
    }).toPromise()
      .then(response => {
        return true;
      }).catch(error => {
        throw new HttpException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.toString(),
          timestamp: new Date().toISOString(),
          path: '/order/confirm-payment',
        }, HttpStatus.INTERNAL_SERVER_ERROR);
      });
  }

  async sendConfirmRegistrationEmail(user: User) {
    const resetPasswordDto = new SendGridTemplate();
    resetPasswordDto.personalizations = [];
    const personalization = new Personalization();
    personalization.to = [];
    const to = new To(user.email);

    resetPasswordDto.from = new Email(supportEmail);
    const encryptedData = CryptoJS.AES.encrypt(
      user.email,
      process.env.CRYPTO_KEY,
    ).toString();
    const buffer = new Buffer(encryptedData);
    const verificationToken = buffer.toString('base64');
    console.log('enctoken', verificationToken);

    personalization.to.push(to);
    personalization.dynamic_template_data = {
      'name': user.username,
      'verificationLink': process.env.HOST + '/verify-email?token=' + verificationToken,
    };
    resetPasswordDto.personalizations.push(personalization);
    resetPasswordDto.template_id = 'd-ce00bec9212c43ab838d33e473fa620d';

    return this.httpService.post(sgUrl, resetPasswordDto, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + process.env.SG_API_KEY,
      },
    }).toPromise()
      .then(response => {
        return true;
      }).catch(error => {
        throw new HttpException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.toString(),
          timestamp: new Date().toISOString(),
          path: '/auth/forgot/password',
        }, HttpStatus.INTERNAL_SERVER_ERROR);
      });
  }


  async sendSucessEmailVerification(user: User) {
    const resetPasswordDto = new SendGridTemplate();
    resetPasswordDto.personalizations = [];
    const personalization = new Personalization();
    personalization.to = [];
    const to = new To(user.email);

    resetPasswordDto.from = new Email(supportEmail);
    personalization.to.push(to);
    personalization.dynamic_template_data = {
      'name': user.username,
      'allRooms': process.env.HOST + '/all-rooms',
    };
    resetPasswordDto.personalizations.push(personalization);
    resetPasswordDto.template_id = 'd-25cbf24eb0224e62b1429bfc5a36e88d';

    return this.httpService.post(sgUrl, resetPasswordDto, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + process.env.SG_API_KEY,
      },
    }).toPromise()
      .then(response => {
        return true;
      }).catch(error => {
        throw new HttpException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.toString(),
          timestamp: new Date().toISOString(),
          path: '/auth/forgot/password',
        }, HttpStatus.INTERNAL_SERVER_ERROR);
      });
  }
}
