export class SendGridTemplate {
  from: Email;
  personalizations: Personalization[];
  template_id: string;
}

export class Email {
  email: string;

  constructor(email: string) {
    this.email = email;
  }
}

export class To {
  email: string;

  constructor(email: string) {
    this.email = email;
  }
}

export class Personalization {
  to: To[];
  sub: any;
  dynamic_template_data: any = {};
}
