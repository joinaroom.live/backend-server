import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { User } from 'src/user/entity/user.entity';
import * as sgMail from '@sendgrid/mail';
import { UserDto } from './user.dto';
import { NotificationsService } from '../notifications/notifications.service';


@Injectable()
export class AuthService {

  private user: User;

  constructor(
    private jwtService: JwtService,
    private usersService: UserService,
    private notificationsService: NotificationsService) {
    sgMail.setApiKey(process.env.SG_API_KEY);
  }

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(email);
    if (user && (await AuthService.passwordsAreEqual(user.password, pass))) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }


  async login(user: any): Promise<any> {
    const authUser = await this.usersService.findOne(user.email);
    const payload = { sub: authUser.id, email: authUser.email, role: authUser.role };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async register(user: User): Promise<any> {
    const newUser = await this.usersService.saveUser(user);

    await this.notificationsService.sendConfirmRegistrationEmail(newUser);
    return this.login(newUser);
  }

  private static async passwordsAreEqual(
    hashedPassword: string,
    plainPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compare(plainPassword, hashedPassword);
  }

  async forgotPasswordEmail(email: string) {
    const user = await this.usersService.findOne(email);

    if (user) {
      await this.notificationsService.sendPasswordResetEmail(user);

      return true;
    } else {
      throw new HttpException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: 'Could not find user with that email',
        timestamp: new Date().toISOString(),
        path: '/auth/forgot/password',
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async resetPassword(userDto: UserDto) {
    const user = await this.usersService.findOne(userDto.email);

    if (user) {
      user.password = await bcrypt.hash(userDto.password, 10);
      await this.usersService.saveUser(user);
      return true;
    } else {
      throw new HttpException({
        status: HttpStatus.UNAUTHORIZED,
        eror: 'Sorry that user doesn\'t exist on our service',
        timestamp: new Date().toISOString(),
        path: 'auth/reset/password',
      }, HttpStatus.UNAUTHORIZED);

    }
  }

}
